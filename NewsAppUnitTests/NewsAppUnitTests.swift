//
//  NewsAppUnitTests.swift
//  NewsAppUnitTests
//
//  Created by Ravi Bastola on 11/6/20.
//

import XCTest
@testable import NewsApp
import Combine

class NewsAppUnitTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        subscription.removeAll()
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    
    var subscription: Set<AnyCancellable> = []
    
    func test_that_network_request_fails_when_the_url_is_invalid() {
        
        class MockURLSession: URLSession {

          var data: Data?
          var response: URLResponse?
          var error: Error?

          init(data: Data?, response: URLResponse?, error: Error?) {
            self.data = data
            self.response = response
            self.error = error
          }

          override func dataTask(with request: URLRequest,
                                 completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> URLSessionDataTask {
            let data = self.data
            let response = self.response
            let error = self.error
            return MockURLSessionDataTask {
              completionHandler(data, response, error)
            }
          }
        }
        
        class MockURLSessionDataTask: URLSessionDataTask {
          private let closure: () -> Void

          init(closure: @escaping () -> Void) {
            self.closure = closure
          }

          override func resume() {
            closure()
          }
        }
        
        NetworkManager.shared.session = MockURLSession.shared
        
        NetworkManager.shared.sendRequest(to: "fake url", model: Everything.self, queryItems: nil).receive(on: RunLoop.main).sink { (completion) in
            switch completion {
            case .failure(let error):
                XCTAssertNotNil(error)
            case .finished:
                break
            }
        } receiveValue: { (model) in
            XCTAssertNil(model)
        }.store(in: &subscription)

    
    }

}
